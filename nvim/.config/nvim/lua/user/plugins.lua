local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system({
    'git', 'clone', '--depth', '1',
    'https://github.com/wbthomason/packer.nvim', install_path
  })
  print('Installing packer close and reopen Neovim...')
  vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, 'packer')
if not status_ok then return end

-- Have packer use a popup window
packer.init({
  display = {
    open_fn = function()
      return require('packer.util').float({ border = 'rounded' })
    end
  }
})

-- Install your plugins here
return packer.startup(function(use)
  -- My plugins here
  use('wbthomason/packer.nvim')
  use('moll/vim-bbye')
  use {
    'goolord/alpha-nvim',
    requires = { 'kyazdani42/nvim-web-devicons' },
    config = function()
      require 'alpha'.setup(require 'alpha.themes.startify'.config)
    end
  }
  use { 'nvim-telescope/telescope-ui-select.nvim' }
  use('tpope/vim-sensible')
  use('tpope/vim-surround')
  use('tpope/vim-repeat')
  use {
    'akinsho/bufferline.nvim',
    tag = 'v2.*',
    requires = 'kyazdani42/nvim-web-devicons'
  }
  use('unblevable/quick-scope')
  use('Shatur/neovim-session-manager')
  use('justinmk/vim-sneak')
  use('lukas-reineke/lsp-format.nvim')
  use('numToStr/Comment.nvim')
  use('joao-vitor-sr/nvimDvorak') -- dvorak keyboard
  use('neovim/nvim-lspconfig')
  use({ 'rafamadriz/friendly-snippets', requires = { 'L3MON4D3/LuaSnip' } })
  use({
    'folke/trouble.nvim',
    requires = 'kyazdani42/nvim-web-devicons',
    config = function()
      require('trouble').setup({
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      })
    end
  })

  use({ "windwp/nvim-autopairs" }) -- Autopairs, integrates with both cmp and treesitter
  use({
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp-signature-help', 'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path', 'hrsh7th/cmp-cmdline', 'onsails/lspkind-nvim',
      'saadparwaiz1/cmp_luasnip', 'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-calc'
    }
  })
  use('tami5/lspsaga.nvim')

  -- files
  use('kyazdani42/nvim-tree.lua')
  use({ 'nvim-telescope/telescope.nvim', requires = { 'nvim-lua/plenary.nvim' } })

  -- theme
  use('shaunsingh/nord.nvim')
  use('folke/tokyonight.nvim')
  use('morhetz/gruvbox')
  use('overcache/NeoSolarized')
  use('nvim-lualine/lualine.nvim')
  use({ "williamboman/nvim-lsp-installer" }) -- simple to use language server installer
  use({ 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' })
  use('norcalli/nvim-colorizer.lua')
  use "ahmedkhalf/project.nvim"
  use "JoosepAlviste/nvim-ts-context-commentstring"
  use 'lewis6991/impatient.nvim'
  use('p00f/nvim-ts-rainbow')
  use "lukas-reineke/indent-blankline.nvim"
  use "lewis6991/gitsigns.nvim"
  if PACKER_BOOTSTRAP then require('packer').sync() end
end)
