vim.cmd [[
try
  colorscheme NeoSolarized 
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
  set background=dark
endtry
]]

vim.api.nvim_command('let g:qs_max_chars=150')
vim.api.nvim_command('let g:sneak#label = 1')
