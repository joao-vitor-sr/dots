local options = {
    termguicolors = true,
    encoding = 'UTF-8',
    fileencoding = 'utf-8',
    number = true,
    relativenumber = true,
    numberwidth = 4,
    colorcolumn = "80",
    tabstop = 2,
    shiftwidth = 2,
    expandtab = true,
    smarttab = true,
    hlsearch = true, -- highlight all matches on previous search pattern
    ignorecase = true, -- ignore case in search patterns
    mouse = 'a', -- allow the mouse to be used in neovim
    conceallevel = 0, -- so that `` is visible in markdown files
    smartcase = true, -- smart case
    clipboard = 'unnamedplus', -- allows neovim to access the system clipboard

    completeopt = { 'menu', 'menuone', 'noselect' } -- mostly just for cmp
}

vim.opt.shortmess:append 'c'

for k, v in pairs(options) do vim.opt[k] = v end

vim.cmd 'set whichwrap+=<,>,[,],h,l'
vim.cmd [[set iskeyword+=-]]
vim.cmd [[set formatoptions-=cro]]
