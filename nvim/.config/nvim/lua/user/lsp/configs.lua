local status_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
if not status_ok then
  return
end

local lspconfig = require("lspconfig")

local servers = {
  'tsserver', 'eslint', 'clangd', 'hls', 'rust_analyzer', 'bashls', 'jsonls',
  'volar', 'sumneko_lua', 'intelephense', 'efm'
}
lsp_installer.setup({
  ensure_installed = servers,
})

for _, server in pairs(servers) do
  local opts = {
    on_attach = require("user.lsp.handlers").on_attach,
    capabilities = require("user.lsp.handlers").capabilities,
  }
  local has_custom_opts, server_custom_opts = pcall(require, "user.lsp.settings." .. server)
  if has_custom_opts then
    opts = vim.tbl_deep_extend("force", opts, server_custom_opts)
  end
  lspconfig[server].setup(opts)
end


local keymap = vim.api.nvim_set_keymap
local opts = { silent = true, noremap = true }

keymap('n', '<leader>xx', '<cmd>TroubleToggle<cr>', opts)
keymap('n', '<leader>xw', '<cmd>Trouble workspace_diagnostics<cr>', opts)
keymap('n', '<leader>xd', '<cmd>Trouble document_diagnostics<cr>', opts)
keymap('n', '<leader>xl', '<cmd>Trouble loclist<cr>', opts)
keymap('n', '<leader>xq', '<cmd>Trouble quickfix<cr>', opts)
keymap('n', 'gR', '<cmd>Trouble lsp_references<cr>', opts)
