require'lsp-format'.setup {
    typescript = {},
    html = {},
    typescriptreact = {},
    vue = {},
    yaml = {},
    lua = {},
    markdown = {},
    plaintex = {},
    tex = {},
    c = {}
}

local prettier = {
    formatCommand = [[prettier --stdin-filepath ${INPUT}]],
    formatStdin = true
}

local lua_format = {
    formatCommand = [[lua-format --double-quote-to-single-quote -i]],
    formatStdin = true
}

local clang_format = {
    formatCommand = [[clang-format -assume-filename ${INPUT}]],
    formatStdin = true
}

return {
    on_attach = require'lsp-format'.on_attach,
    init_options = {documentFormatting = true},
    settings = {
        languages = {
            typescript = {prettier},
            typescriptreact = {prettier},
            html = {prettier},
            yaml = {prettier},
            markdown = {prettier},
            vue = {prettier},
            c = {clang_format},
            lua = {lua_format},
            plaintex = {prettier},
            tex = {prettier}
        }
    }
}
