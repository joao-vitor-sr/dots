local lspsaga = require('lspsaga')
lspsaga.setup({
    -- defaults ...
    debug = false,
    use_saga_diagnostic_sign = true,
    -- diagnostic sign
    error_sign = '',
    warn_sign = '',
    hint_sign = '',
    infor_sign = '',
    -- code action title icon
    code_action_icon = ' ',
    code_action_prompt = {
        enable = true,
        sign = true,
        sign_priority = 40,
        virtual_text = true
    },
    finder_definition_icon = '  ',
    finder_reference_icon = '  ',
    max_preview_lines = 10,
    finder_action_keys = {
        open = 'o',
        vsplit = 's',
        split = 'i',
        quit = 'q',
        scroll_down = '<C-f>',
        scroll_up = '<C-b>'
    },
    code_action_keys = {quit = 'q', exec = '<CR>'},
    rename_action_keys = {quit = '<C-c>', exec = '<CR>'},
    definition_preview_icon = '  ',
    border_style = 'single',
    rename_prompt_prefix = '➤',
    server_filetype_map = {},
    diagnostic_prefix_format = '%d. '
})

vim.api.nvim_set_keymap('n', '<leader>e',
                        '<cmd>Lspsaga show_line_diagnostics<CR>',
                        {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<leader>rn', '<cmd>Lspsaga rename<cr>',
                        {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<leader>ca', '<cmd>Lspsaga code_action<cr>',
                        {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<leader>gr', '<cmd>Lspsaga lsp_finder<cr>',
                        {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '[d', '<cmd>Lspsaga diagnostic_jump_next<CR>',
                        {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', ']d', '<cmd>Lspsaga` diagnostic_jump_prev<CR>',
                        {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'K', '<cmd>Lspsaga hover_doc<CR>',
                        {noremap = true, silent = true})
